//
//  HomeController.swift
//  Pokemon_RxMVVMC_practice
//
//  Created by Randy Flores on 10/16/20.
//

import UIKit
import PKHUD
import DropDown

import RxSwift
import RxCocoa
import RxDataSources


class PokemonsController: UIViewController {
  
  let tableView = UITableView()
  let dropDown = DropDown()
  let dropDownButton = UIButton()
  var safeArea: UILayoutGuide!
  let screenSize: CGRect = UIScreen.main.bounds
  
  var choices = [
    "Sort results by...","Lowest Number (First)",
    "Highest Number (First)", "A-Z", "Z-A"]
  var pokemons : [Pokemon] = []
  let cellIdentifier = "pokemonCell"
  
  var viewModel: PokemonsViewModel!
  private let disposeBag = DisposeBag()
  
  // MARK: - Lifecycle Methods
   override func viewWillAppear(_ animated: Bool) {
     super.viewWillAppear(animated)
    safeArea = view.layoutMarginsGuide
    configureTable()
    configureDropDown()
   }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    setTableConstraints()
    setDropDownConstraints()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = .white
    navigationController?.navigationBar
      .prefersLargeTitles = true
    navigationItem.title = "Home"
    bindTableView()
    bindLoading()
    
    
    viewModel.fetchPokemons{ (errorMessage) in
      self.showMessage(errorMessage)
    }
  }
}

// MARK: - Bindings
extension PokemonsController {
  func bindTableView() {
    
    viewModel.pokemons
      .bind(to: tableView.rx.items(cellIdentifier:
        cellIdentifier, cellType: PokemonsCell.self)) {
        index, viewModel, cell in
          cell.viewModel = viewModel
      }
      .disposed(by: disposeBag)
    
    tableView.rx.modelSelected(PokemonViewModel.self)
      .bind(to: viewModel.selectPokemon)
      .disposed(by: disposeBag)
    
    tableView.rx.itemSelected
      .subscribe(onNext: { (indexPath) in
          self.tableView.deselectRow(
            at: indexPath, animated: true)})
      .disposed(by: disposeBag)
  }
  
  func bindLoading() {
    viewModel.isLoading
      .subscribe(onNext: { [weak self] isLoading in
        isLoading ? self?.showProgress() :
          self?.hideProgress()
      })
      .disposed(by: disposeBag)
  }
}

// MARK: - TableView
extension PokemonsController {
  
  func configureTable() {
    tableView.bounces = false
    tableView.rowHeight = screenSize.height * 0.5
    tableView.register(PokemonsCell.self,
      forCellReuseIdentifier: cellIdentifier)
    view.addSubview(tableView)
  }
  
  func setTableConstraints(){
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.topAnchor.constraint(equalTo:
      dropDownButton.bottomAnchor).isActive = true
    tableView.leadingAnchor.constraint(equalTo:
      view.leadingAnchor).isActive = true
    tableView.trailingAnchor.constraint(equalTo:
      view.trailingAnchor).isActive = true
    tableView.bottomAnchor.constraint(equalTo:
      view.bottomAnchor).isActive = true
  }
}

// MARK: - DropDown
extension PokemonsController {
  
  func configureDropDown() {
    dropDownButton.addTarget(self,
      action: #selector(self.tapChooseMenuItem(_:)),
        for: .touchUpInside)
    dropDownButton.backgroundColor = .gray
    dropDownButton.setTitle(choices[0], for: .normal)
    dropDownButton.contentHorizontalAlignment = .left
    dropDownButton.contentEdgeInsets
      = UIEdgeInsets(top: 5,left: 20,bottom: 5,right: 20)
    view.addSubview(dropDownButton)
  }
  
  func setDropDownConstraints() {
    dropDownButton.translatesAutoresizingMaskIntoConstraints = false
    dropDownButton.topAnchor.constraint(equalTo:
      safeArea.topAnchor).isActive = true
    dropDownButton.leadingAnchor.constraint(equalTo:
      view.leadingAnchor).isActive = true
    dropDownButton.trailingAnchor.constraint(equalTo:
      view.trailingAnchor).isActive = true
    dropDownButton.heightAnchor.constraint(equalToConstant:
      44).isActive = true
  }
  
  @objc func tapChooseMenuItem(_ sender: UIButton) {
    dropDown.dataSource = choices
    dropDown.anchorView = sender
    dropDown.bottomOffset = CGPoint(x: 0,
      y: sender.frame.size.height)
    dropDown.show()
    dropDown.selectionAction = {[weak self]
      (index: Int, item: String) in
      guard let _ = self else { return }
      sender.setTitle(item, for: .normal)
    }
  }
}
