//
//  PokemonController.swift
//  Pokemon_RxMVVMC_practice
//
//  Created by Randy Flores on 10/16/20.
//

import UIKit
import RxSwift

class PokemonController: UIViewController {
  
  // MARK: - Variables
  lazy var abilityStack : UIStackView = {
    let stack = UIStackView()
    stack.axis = .vertical
    stack.alignment = .leading
    stack.distribution = .fillEqually
    return stack
  }()
  
  lazy var abilityLabel : UILabel = {
    let text = UILabel()
    text.adjustsFontSizeToFitWidth = true
    text.numberOfLines = 1
    text.font = text.font.withSize(16)
    text.minimumScaleFactor = 0.1
    text.textColor = .white
    text.textAlignment = .left
    text.sizeToFit()
    return text
  }()
  
  lazy var abilityText : UILabel = {
    let text = UILabel()
    text.adjustsFontSizeToFitWidth = true
    text.numberOfLines = 2
    text.font = text.font.withSize(18)
    text.minimumScaleFactor = 0.1
    text.textAlignment = .left
    text.textColor = .darkGray
    return text
  }()
  
  lazy var backgroundView : UIView = {
    let view = UIView()
    view.backgroundColor = .systemGray6
    view.layer.cornerRadius = 10
    return view
  }()
  
  lazy var detailsView : UIView = {
    let view = UIView()
    view.backgroundColor = .white
    view.layer.cornerRadius = 10
    return view
  }()
  
  lazy var heightWeightAbilityStack : UIStackView = {
    let stack = UIStackView()
    stack.axis = .vertical
    stack.alignment = .fill
    stack.distribution = .fillEqually
    return stack
  }()
  
  lazy var heightWeightAbilityView : UIView = {
    let view = UIView()
    view.backgroundColor = UIColor(named: "detailblue") ?? .systemTeal
    view.layer.cornerRadius = 10
    return view
  }()
  
  lazy var heightAndWeightStack : UIStackView = {
    let stack = UIStackView()
    stack.distribution = .fillEqually
    stack.alignment = .fill
    stack.axis = .horizontal
    return stack
  }()
  
  lazy var heightStack : UIStackView = {
    let stack = UIStackView()
    stack.axis = .vertical
    stack.distribution = .fillEqually
    stack.alignment = .fill
    return stack
  }()
  
  lazy var weightStack : UIStackView = {
    let stack = UIStackView()
    stack.axis = .vertical
    stack.distribution = .fillEqually
    stack.alignment = .fill
    return stack
  }()
  
  lazy var heightLabel : UILabel = {
    let text = UILabel()
    text.adjustsFontSizeToFitWidth = true
    text.numberOfLines = 1
    text.font = text.font.withSize(16)
    text.minimumScaleFactor = 0.1
    text.textColor = .white
    text.textAlignment = .left
    text.sizeToFit()
    return text
  }()
  
  lazy var weightLabel : UILabel = {
    let text = UILabel()
    text.adjustsFontSizeToFitWidth = true
    text.numberOfLines = 1
    text.font = text.font.withSize(16)
    text.minimumScaleFactor = 0.1
    text.textColor = .white
    text.textAlignment = .left
    text.sizeToFit()
    return text
  }()
  
  lazy var heightText : UILabel = {
    let text = UILabel()
    text.adjustsFontSizeToFitWidth = true
    text.numberOfLines = 1
    text.font = text.font.withSize(18)
    text.minimumScaleFactor = 0.1
    text.textAlignment = .left
    text.textColor = .darkGray
    return text
  }()
  
  lazy var weightText : UILabel = {
    let text = UILabel()
    text.adjustsFontSizeToFitWidth = true
    text.numberOfLines = 1
    text.font = text.font.withSize(18)
    text.minimumScaleFactor = 0.1
    text.textAlignment = .left
    text.textColor = .darkGray
    return text
  }()
  
  lazy var pokemonImage : UIImageView = {
    let image = UIImageView()
    image.layer.cornerRadius = 10
    image.clipsToBounds = true
    image.image = UIImage(named: "pokemonTemp")
    image.contentMode = .scaleAspectFit
    image.backgroundColor = .systemGray6
    
    return image
  }()
  
  //var safeArea: UILayoutGuide!
  let screenSize: CGRect = UIScreen.main.bounds
  
  lazy var scrollView: UIScrollView = {
    let scroll = UIScrollView()
    scroll.backgroundColor = .white
    scroll.bounces = true
    return scroll
  }()
  
  lazy var nameIDText : UILabel = {
    var text = UILabel()
    text.adjustsFontSizeToFitWidth = true
    text.numberOfLines = 1
    text.font = text.font.withSize(25)
    text.minimumScaleFactor = 0.1
    return text
  }()
  
  var viewModel: PokemonViewModel!
  
  lazy var typeLabel : UILabel = {
    var text = UILabel()
    text.adjustsFontSizeToFitWidth = true
    text.numberOfLines = 1
    text.font = text.font.withSize(20)
    text.minimumScaleFactor = 0.1
    return text
  }()
  
  lazy var typeCollection : UICollectionView = {
    let collectionView = UICollectionView(frame: .zero,
      collectionViewLayout: collectionViewLayout())
      collectionView.backgroundColor = .white
      collectionView.translatesAutoresizingMaskIntoConstraints = false
      return collectionView
  }()
  
  // MARK: - Lifecycle Methods
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.view.backgroundColor = .white
    configureScrollView()
    configureBackgroundView()
    configureImage()
    configureDetailsView()
    configureNameIdText()
    configureHeightWeightAbilityView()
    configureHeightWeightAbilityStack()
    configureHeightAndWeightStack()
    configureHeightAndWeightSubView()
    configureHeightAndWeightSubStack()
    configureAbilityStack()
    configureAbilitySubView()
    configureTypeLabel()
    configureTypeCollection()
  }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    setScrollViewConstraints()
    setBackgroundConstraints()
    setImageConstraints()
    setDetailsConstraints()
    setNameIdTextConstraints()
    setHeightWeightAbilityViewConstraints()
    setHeightWeightAbilityStackConstraints()
    setTypeLabelConstraints()
    setTypeCollectionConstraints()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    bindViewModel()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    viewModel.didClose.onNext(())
  }
  
  
  // MARK: - Settings and Constraints
  func configureScrollView() {
    view.addSubview(scrollView)
    scrollView.translatesAutoresizingMaskIntoConstraints = false
    scrollView.alwaysBounceHorizontal = false
  }
  
  func setScrollViewConstraints() {
    scrollView.topAnchor.constraint(equalTo:
      view.topAnchor).isActive = true
    scrollView.bottomAnchor.constraint(equalTo:
      view.bottomAnchor).isActive = true
    scrollView.leadingAnchor.constraint(equalTo:
      view.leadingAnchor).isActive = true
    scrollView.trailingAnchor.constraint(equalTo:
      view.trailingAnchor).isActive = true
  }
  
  func configureBackgroundView() {
    backgroundView.translatesAutoresizingMaskIntoConstraints = false
    scrollView.addSubview(backgroundView)
  }
  
  func setBackgroundConstraints(){
    backgroundView.bottomAnchor.constraint(equalTo:
      scrollView.bottomAnchor, constant: 10).isActive = true
    backgroundView.leadingAnchor.constraint(equalTo:
      scrollView.leadingAnchor, constant: 20)
      .isActive = true
    backgroundView.trailingAnchor.constraint(equalTo:
      scrollView.trailingAnchor, constant: -20)
      .isActive = true
    backgroundView.topAnchor.constraint(equalTo:
      scrollView.topAnchor).isActive = true
    backgroundView.centerYAnchor.constraint(equalTo:
      scrollView.centerYAnchor).isActive = true
    backgroundView.centerXAnchor.constraint(equalTo:
      scrollView.centerXAnchor).isActive = true
  }
  
  func configureImage() {
    backgroundView.addSubview(pokemonImage)
    pokemonImage.translatesAutoresizingMaskIntoConstraints = false
  }
  
  func setImageConstraints(){
    pokemonImage.topAnchor.constraint(equalTo:
      backgroundView.topAnchor).isActive = true
    pokemonImage.leadingAnchor.constraint(equalTo:
      backgroundView.leadingAnchor).isActive = true
    pokemonImage.trailingAnchor.constraint(equalTo:
      backgroundView.trailingAnchor).isActive = true
    pokemonImage.heightAnchor.constraint(
      equalToConstant: screenSize.height * 0.3).isActive = true
  }
  
  func configureDetailsView() {
    backgroundView.addSubview(detailsView)
    detailsView.translatesAutoresizingMaskIntoConstraints = false
  }
  
  func setDetailsConstraints(){
    detailsView.topAnchor.constraint(equalTo:
      pokemonImage.bottomAnchor).isActive = true
    detailsView.bottomAnchor.constraint(equalTo:
      backgroundView.bottomAnchor, constant: -20).isActive = true
    detailsView.leadingAnchor.constraint(equalTo:
      backgroundView.leadingAnchor, constant: 10).isActive = true
    detailsView.trailingAnchor.constraint(equalTo:
      backgroundView.trailingAnchor, constant: -10).isActive = true
  }
  
  func configureNameIdText(){
    nameIDText.adjustsFontSizeToFitWidth = true
    nameIDText.numberOfLines = 1
    nameIDText.textAlignment = .center
    nameIDText.translatesAutoresizingMaskIntoConstraints = false
    detailsView.addSubview(nameIDText)
  }
  
  func setNameIdTextConstraints(){
    nameIDText.topAnchor.constraint(equalTo:
      detailsView.topAnchor).isActive = true
    nameIDText.leadingAnchor.constraint(equalTo:
      detailsView.leadingAnchor).isActive = true
    nameIDText.trailingAnchor.constraint(equalTo:
      detailsView.trailingAnchor).isActive = true
    nameIDText.heightAnchor.constraint(equalToConstant: 55)
      .isActive = true
  }
  
  func configureHeightWeightAbilityView() {
    detailsView.addSubview(heightWeightAbilityView)
    heightWeightAbilityView
      .translatesAutoresizingMaskIntoConstraints = false
  }
  
  func setHeightWeightAbilityViewConstraints() {
    heightWeightAbilityView.topAnchor.constraint(equalTo:
      nameIDText.bottomAnchor,constant: 20).isActive = true
    heightWeightAbilityView.leadingAnchor.constraint(equalTo:
      nameIDText.leadingAnchor, constant: 20).isActive = true
    heightWeightAbilityView.trailingAnchor.constraint(equalTo:
      nameIDText.trailingAnchor, constant: -20).isActive = true
    heightWeightAbilityView.heightAnchor.constraint(equalToConstant:
      120).isActive = true
  }
  
  func configureHeightWeightAbilityStack() {
    heightWeightAbilityView.addSubview(heightWeightAbilityStack)
    heightWeightAbilityStack.translatesAutoresizingMaskIntoConstraints = false
  }
  
  func setHeightWeightAbilityStackConstraints() {
    heightWeightAbilityStack.topAnchor.constraint(equalTo:
      heightWeightAbilityView.topAnchor, constant: 10).isActive = true
    heightWeightAbilityStack.bottomAnchor.constraint(equalTo:
      heightWeightAbilityView.bottomAnchor, constant: -10).isActive = true
    heightWeightAbilityStack.leadingAnchor.constraint(equalTo:
      heightWeightAbilityView.leadingAnchor, constant: 10).isActive = true
    heightWeightAbilityStack.trailingAnchor.constraint(equalTo:
      heightWeightAbilityView.trailingAnchor, constant: -10).isActive = true
  }
  
  func configureHeightAndWeightStack() {
    heightWeightAbilityStack.addArrangedSubview(heightAndWeightStack)
  }
  
  func configureHeightAndWeightSubView() {
    heightAndWeightStack.addArrangedSubview(heightStack)
    heightAndWeightStack.addArrangedSubview(weightStack)
  }
  
  func configureHeightAndWeightSubStack() {
    heightStack.addArrangedSubview(heightLabel)
    heightStack.addArrangedSubview(heightText)
    weightStack.addArrangedSubview(weightLabel)
    weightStack.addArrangedSubview(weightText)
  }
  
  func configureAbilityStack() {
    heightWeightAbilityStack.addArrangedSubview(abilityStack)
  }
  
  func configureAbilitySubView() {
    abilityStack.addArrangedSubview(abilityLabel)
    abilityStack.addArrangedSubview(abilityText)
  }
  
  func configureTypeLabel() {
    detailsView.addSubview(typeLabel)
    typeLabel.translatesAutoresizingMaskIntoConstraints = false
  }
  
  func setTypeLabelConstraints() {
    typeLabel.topAnchor.constraint(equalTo:
      heightWeightAbilityView.bottomAnchor, constant: 10).isActive = true
    typeLabel.leadingAnchor.constraint(equalTo:
      heightWeightAbilityView.leadingAnchor, constant: 10).isActive = true
    typeLabel.trailingAnchor.constraint(equalTo:
      heightWeightAbilityView.trailingAnchor).isActive = true
    typeLabel.heightAnchor.constraint(equalToConstant: 55)
      .isActive = true
  }
  
  func configureTypeCollection() {
    detailsView.addSubview(typeCollection)
    typeCollection.delegate = self
    typeCollection.dataSource = self
    typeCollection.register(TypeCell.self,
      forCellWithReuseIdentifier: "typesCell")
  }
  
  func setTypeCollectionConstraints(){
    typeCollection.leadingAnchor.constraint(
      equalTo: typeLabel.leadingAnchor).isActive = true
    typeCollection.trailingAnchor.constraint(
      equalTo: typeLabel.trailingAnchor).isActive = true
    typeCollection.topAnchor.constraint(
      equalTo: typeLabel.bottomAnchor).isActive = true
    typeCollection.heightAnchor.constraint(
      equalToConstant: 55).isActive = true
  }
  //MARK: - Other Methods
  private func collectionViewLayout() -> UICollectionViewLayout {
    let layout = UICollectionViewFlowLayout()
    let cellWidthHeightConstant: CGFloat = screenSize.width * 0.2
    
    layout.scrollDirection = .vertical
    layout.minimumInteritemSpacing = 0
    layout.minimumLineSpacing = 0
    layout.itemSize = CGSize(
      width: cellWidthHeightConstant, height: 50)
    
    return layout
  }
}

// MARK: - Binding
extension PokemonController {
  func bindViewModel() {
    self.nameIDText.text = viewModel.name + " " + viewModel.id
    self.pokemonImage.load(url: viewModel.image)
    self.heightLabel.text = "Height"
    self.weightLabel.text = "Weight"
    self.heightText.text = viewModel.height
    self.weightText.text = viewModel.weight
    self.abilityLabel.text = "Abilities"
    self.abilityText.text = viewModel.abilities.joined(separator: ", ")
    self.typeLabel.text = "Type"
  }
}

//MARK: - TypeCollection
extension PokemonController : UICollectionViewDelegate,
  UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView,
    numberOfItemsInSection section: Int) -> Int {
    return viewModel.types.count
  }
  
  func collectionView(_ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let collectionCell = collectionView.dequeueReusableCell(
      withReuseIdentifier: "typesCell", for: indexPath) as! TypeCell
    
    collectionCell.type = viewModel.types[indexPath.item]
    return collectionCell
  }
}
