//
//  HomeCell.swift
//  Pokemon_RxMVVMC_practice
//
//  Created by Randy Flores on 10/16/20.
//

import UIKit

import RxSwift

class PokemonsCell: UITableViewCell {
  
  lazy var detailStack : UIStackView = {
    let stack = UIStackView()
    stack.alignment = .fill
    stack.axis = .vertical
    stack.distribution = .fillProportionally
    stack.translatesAutoresizingMaskIntoConstraints = false
    return stack
  }()
  
  lazy var idText : UILabel = {
    let text = UILabel()
    text.adjustsFontSizeToFitWidth = true
    text.numberOfLines = 1
    text.font = text.font.withSize(16)
    text.textColor = .gray
    return text
  }()
  
  lazy var nameText : UILabel = {
    let text = UILabel()
    text.adjustsFontSizeToFitWidth = true
    text.numberOfLines = 1
    text.font = text.font.withSize(25)
    text.minimumScaleFactor = 0.1
    return text
  }()
  
  lazy var pokemonImage : UIImageView = {
    let image = UIImageView()
    image.layer.cornerRadius = 10
    image.clipsToBounds = true
    image.image = UIImage(named: "pokemonTemp")
    image.contentMode = .scaleAspectFit
    image.translatesAutoresizingMaskIntoConstraints = false
    return image
  }()
  
  lazy var imageBackground: UIView = {
    let view = UIView()
    view.layer.cornerRadius = 10
    view.layer.backgroundColor = UIColor.systemGray6.cgColor
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  lazy var typeCollection : UICollectionView = {
    let collectionView = UICollectionView(frame: .zero,
      collectionViewLayout: collectionViewLayout())
      collectionView.backgroundColor = .white
      collectionView.translatesAutoresizingMaskIntoConstraints = false
      return collectionView
  }()
  
  let screenSize: CGRect = UIScreen.main.bounds
  
  let disposeBag = DisposeBag()
  
  var viewModel: PokemonViewModel! {
    didSet {
      self.configure()
    }
  }
  
  override init(style: UITableViewCell.CellStyle,
    reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    addSubview(imageBackground)
    addSubview(pokemonImage)
    addSubview(detailStack)
    addSubview(idText)
    addSubview(nameText)
    addSubview(typeCollection)
    typeCollection.delegate = self
    typeCollection.dataSource = self
    typeCollection.register(TypeCell.self,
      forCellWithReuseIdentifier: "typesCell")
    
    bringSubviewToFront(pokemonImage)
    
    configureImage()
    configureDetailStack()
    configureTypeCollection()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func configure() {
    pokemonImage.image = nil
    pokemonImage.load(url: viewModel.image)
    idText.text = viewModel.id
    nameText.text = viewModel.name
    typeCollection.reloadData()
  }
    
  func configureImage() {
    imageBackground.translatesAutoresizingMaskIntoConstraints
     = false
    imageBackground.topAnchor.constraint(equalTo:
      topAnchor, constant: (screenSize.height * 0.5) * 0.05)
      .isActive = true
    imageBackground.centerXAnchor.constraint(equalTo:
      centerXAnchor).isActive = true
    imageBackground.bottomAnchor.constraint(
      equalTo: detailStack.topAnchor, constant: -5).isActive = true
    imageBackground.widthAnchor.constraint(equalToConstant:
      screenSize.width * 0.70).isActive = true

    
    pokemonImage.widthAnchor.constraint(equalToConstant:
      (screenSize.width * 0.70) * 0.5).isActive = true
    pokemonImage.heightAnchor.constraint(equalToConstant:
      (screenSize.width * 0.70) * 0.5).isActive = true
    pokemonImage.centerXAnchor.constraint(equalTo:
      imageBackground.centerXAnchor).isActive = true
    pokemonImage.centerYAnchor.constraint(equalTo:
      imageBackground.centerYAnchor).isActive = true
    
  }
  
  func configureDetailStack() {
    detailStack.heightAnchor.constraint(
      equalToConstant: (screenSize.height * 0.5) * 0.2)
      .isActive = true
    detailStack.leadingAnchor.constraint(equalTo:
      imageBackground.leadingAnchor, constant: 10).isActive = true
    detailStack.trailingAnchor.constraint(equalTo:
      imageBackground.trailingAnchor, constant: -10).isActive = true
    detailStack.bottomAnchor.constraint(
      equalTo: typeCollection.topAnchor).isActive = true
    detailStack.addArrangedSubview(idText)
    detailStack.addArrangedSubview(nameText)
  }
  
  func configureTypeCollection(){
    typeCollection.register(TypeCell.self,
      forCellWithReuseIdentifier: "typesCell")
    typeCollection.leadingAnchor.constraint(
      equalTo: imageBackground.leadingAnchor).isActive = true
    typeCollection.trailingAnchor.constraint(
      equalTo: imageBackground.trailingAnchor).isActive = true
    typeCollection.bottomAnchor.constraint(
      equalTo: bottomAnchor, constant: -5).isActive = true
    typeCollection.heightAnchor.constraint(
      equalToConstant: (screenSize.height * 0.5) * 0.15).isActive = true
  }
  
  private func collectionViewLayout() -> UICollectionViewLayout {
    let layout = UICollectionViewFlowLayout()
    let cellWidthHeightConstant: CGFloat = screenSize.width * 0.2
    
    layout.scrollDirection = .vertical
    layout.minimumInteritemSpacing = 0
    layout.minimumLineSpacing = 0
    layout.itemSize = CGSize(
      width: cellWidthHeightConstant, height: 50)
    
    return layout
  }
}

//MARK: - Bindings
extension PokemonsCell : UICollectionViewDelegate,
  UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView,
    numberOfItemsInSection section: Int) -> Int {
    return viewModel.types.count
  }
  
  func collectionView(_ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let collectionCell = collectionView.dequeueReusableCell(
      withReuseIdentifier: "typesCell", for: indexPath) as! TypeCell
    
    collectionCell.type = viewModel.types[indexPath.item]
    return collectionCell
  }
}
