//
//  TypeCell.swift
//  Pokemon_RxMVVMC_practice
//
//  Created by Randy Flores on 10/20/20.
//

import UIKit

class TypeCell: UICollectionViewCell {
  
  lazy var roundedBackgroundView: UIView = {
    let view = UIView()
    view.layer.cornerRadius = 10
    view.layer.borderColor = UIColor.systemGray.cgColor
    view.layer.borderWidth = 1
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }()
  
  lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.font = UIFont(name: "HelveticaNeue", size: 14)
    label.textColor = .systemBlue
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()
  
  var type: String! {
    didSet {
      titleLabel.text = nil
      guard let type = type else {return}
      titleLabel.text = type
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    setupUI()
  }
  
}

extension TypeCell {
  private func setupUI() {
    self.contentView.addSubview(roundedBackgroundView)
    roundedBackgroundView.addSubview(titleLabel)
    
    NSLayoutConstraint.activate([
        roundedBackgroundView.topAnchor.constraint(
          equalTo: self.contentView.topAnchor, constant: 5),
        roundedBackgroundView.leftAnchor.constraint(
          equalTo: self.contentView.leftAnchor, constant: 5),
        roundedBackgroundView.rightAnchor.constraint(
          equalTo: self.contentView.rightAnchor, constant: -5),
      roundedBackgroundView.heightAnchor.constraint(
        equalToConstant: 30),
        titleLabel.centerXAnchor.constraint(
          equalTo: roundedBackgroundView.centerXAnchor),
        titleLabel.centerYAnchor.constraint(
          equalTo: roundedBackgroundView.centerYAnchor)
    ])
      
  }
}
