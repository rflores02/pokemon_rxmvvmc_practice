//
//  Extensions.swift
//  Pokemon_RxMVVMC_practice
//
//  Created by Randy Flores on 10/19/20.
//

import PKHUD

extension UIViewController {
  func showProgress() {
    HUD.show(.progress)
  }
  
  func hideProgress() {
    HUD.hide()
  }
  
  func showMessage(_ message: String) {
    HUD.flash(.labeledError(title: nil, subtitle: message), delay: 1.5)
  }
}

extension UIImageView {
  func load(url: URL) {
    DispatchQueue.global().async {
      [weak self] in
      if let data = try? Data(contentsOf: url) {
        if let image = UIImage(data: data) {
          DispatchQueue.main.async {
            self?.image = image
          }
        }
      }
    }
  }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
