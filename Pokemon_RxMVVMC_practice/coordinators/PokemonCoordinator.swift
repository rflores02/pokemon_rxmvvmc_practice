//
//  PokemonDetailCoordinator.swift
//  Pokemon_RxMVVMC_practice
//
//  Created by Randy Flores on 10/19/20.
//

import RxSwift

class PokemonCoordinator: BaseCoordinator<Void> {
  
  private let rootViewController: UIViewController
  public var viewModel: PokemonViewModel!
  
  init(rootViewController: UIViewController) {
    self.rootViewController = rootViewController
  }
  
  override func start() -> Observable<Void> {
    let viewController = PokemonController()
    viewController.viewModel = viewModel
    
    let didClose = viewModel.didClose
    
    rootViewController.navigationController?
      .pushViewController(viewController, animated: true)
    
    return didClose.take(1)
  }
}
