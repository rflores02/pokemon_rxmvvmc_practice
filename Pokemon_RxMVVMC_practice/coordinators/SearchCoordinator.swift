//
//  SearchCoordinator.swift
//  Pokemon_RxMVVMC_practice
//
//  Created by Randy Flores on 10/16/20.
//

import RxSwift

class SearchCoordinator : BaseCoordinator<Void> {
  
  private let navigationController: UINavigationController
  
  init(navigationController: UINavigationController){
    self.navigationController = navigationController
  }
  
  override func start() -> Observable<Void>{
    let viewController = SearchController()
    navigationController.setViewControllers(
      [viewController], animated: true)
    return Observable.empty()
  }
}
