//
//  HomeCoordinator.swift
//  Pokemon_RxMVVMC_practice
//
//  Created by Randy Flores on 10/16/20.
//

import RxSwift

class PokemonsCoordinator : BaseCoordinator<Void> {
  
  private let navigationController: UINavigationController
  
  init(navigationController: UINavigationController){
    self.navigationController = navigationController
  }
  
  override func start() -> Observable<Void>{
    let viewController = PokemonsController()
    let viewModel = PokemonsViewModel()
    viewController.viewModel = viewModel
    
    viewModel.selectPokemon
      .flatMap({ [unowned self] (pokemonViewModel) in
        self.coordinateToPokemonDetail(with: pokemonViewModel)
      })
      .subscribe()
      .disposed(by: disposeBag)
    
    navigationController.setViewControllers(
      [viewController], animated: true)
    return Observable.never()
  }
  
  // MARK: - Coordination
  private func coordinateToPokemonDetail(with
    pokemonViewModel: PokemonViewModel) -> Observable<Void> {
    let pokemonCoordinator = PokemonCoordinator(
      rootViewController: navigationController.viewControllers[0])
    pokemonCoordinator.viewModel = pokemonViewModel
    
    return coordinate(to: pokemonCoordinator)
  }
}
