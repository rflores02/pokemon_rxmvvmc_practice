//
//  AppDelegate.swift
//  Pokemon_RxMVVMC_practice
//
//  Created by Randy Flores on 10/16/20.
//

import UIKit
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  private var appCoordinator: AppCoordinator!
  private let disposeBag = DisposeBag()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
      window = UIWindow()
      
      appCoordinator = AppCoordinator(window: window!)
      appCoordinator.start()
        .subscribe()
        .disposed(by: disposeBag)
      
      return true
    }

}

