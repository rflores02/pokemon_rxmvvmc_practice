//
//  PokemonDetailViewModel.swift
//  Pokemon_RxMVVMC_practice
//
//  Created by Randy Flores on 10/19/20.
//

import RxSwift

struct PokemonViewModel {
  
  let didClose = PublishSubject<Void>()
  
  let id: String
  let name: String
  let types : [String]
  let image : URL
  let height : String
  let weight : String
  let abilities : [String]
  
  init(pokemon: Pokemon){
    self.id = pokemon.id
    self.name = pokemon.name
    self.types = pokemon.types
    self.image = pokemon.image
    self.height = pokemon.height
    self.weight = pokemon.weight
    self.abilities = pokemon.abilities
  }
}
