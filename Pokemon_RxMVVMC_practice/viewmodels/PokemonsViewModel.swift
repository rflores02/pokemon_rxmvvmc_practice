//
//  PokemonsViewModel.swift
//  Pokemon_RxMVVMC_practice
//
//  Created by Randy Flores on 10/19/20.
//

import RxSwift
import RxCocoa

import PokemonAPI
import Combine

class PokemonsViewModel {
  private let disposeBag = DisposeBag()
  let pokemonApi = PokemonAPI()
  var cancellables = Set<AnyCancellable>()
  private var pokemonItems : [PokemonViewModel] = []
  
  //MARK: - Actions
  let isLoading = BehaviorSubject<Bool>(value: false)
  let selectPokemon = PublishSubject<PokemonViewModel>()
  
  
  //MARK: - Table View Model and Data Source
  var pokemons = BehaviorSubject<[PokemonViewModel]>(
    value: []
  )
  
  func customPokemonImage(id : Int) -> URL {
    let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/\(String(id)).png"
    return URL(string: url)!
  }
  
  func modifyIdToString(id : Int) -> String {
    var stringId = String(id)
    
    if (stringId.count == 1) {
      stringId = "#00" + stringId
    }else if (stringId.count == 2){
      stringId = "#0" + stringId
    }else if (stringId.count == 3){
      stringId = "#" + stringId
    }else {
      stringId = "#" + stringId
    }
    return stringId
  }
  
  func modifyHeightToString(height : Int) -> String{
    return String(height) + "ft"
  }
  
  func modifyWeightToString(weight : Int) -> String{
    return String(weight) + "lbs"
  }
  
  //MARK: - API Call
  func fetchPokemons(onError: @escaping (String) -> ()){
    
    self.isLoading.onNext(true)
    pokemonApi.pokemonService.fetchPokemonList(
      paginationState: .initial(pageLimit: 10)) {
      result in
      
      switch result {
      case .success(let pagedObject):
        for pokemonObject in (pagedObject.results)!
          as [PKMAPIResource<PKMPokemon>] {
          if let pokemon = pokemonObject
            as? PKMNamedAPIResource {
            self.pokemonApi.pokemonService.fetchPokemon(
              pokemon.name! as String) {
              pokemonResult in
              
              switch pokemonResult {
              case .success(let myPokemon):
                var myTypes : [String] = []
                var myAbilities : [String] = []
                
                for typeUnwrapped in (myPokemon.types)! as
                  [PKMPokemonType] {
                  
                  if let myType = typeUnwrapped.type {
                    myTypes.append(myType.name!.capitalizingFirstLetter())
                  }
                }
                
                for abilitiesUnwrapped in (myPokemon.abilities)! as
                  [PKMPokemonAbility] {
                  
                  if let myAbility = abilitiesUnwrapped.ability {
                    myAbilities.append(myAbility.name!.capitalizingFirstLetter())
                  }
                }
                
                self.pokemonItems.append(PokemonViewModel(
                  pokemon: Pokemon(
                    id: self.modifyIdToString(id:myPokemon.id!),
                    name: (myPokemon.name!).capitalizingFirstLetter(),
                    types: myTypes,
                    image: self.customPokemonImage(id: myPokemon.id!),
                    height: self.modifyHeightToString(height: myPokemon.height!),
                    weight: self.modifyWeightToString(weight: myPokemon.weight!),
                    abilities: myAbilities
                  )
                ))
              case .failure(let error):
                print("error \(error.localizedDescription)")
              }
              if (self.pokemonItems.count ==
                pagedObject.results?.count){
                DispatchQueue.main.async {
                  self.pokemons.onNext(self.pokemonItems)
                  self.isLoading.onNext(false)
                }
              }
            }
          }
        }
      case .failure(let error):
        print("error: \(error.localizedDescription)")
      }
    }
  }
}
