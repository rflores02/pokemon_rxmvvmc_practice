//
//  Pokemon.swift
//  Pokemon_RxMVVMC_practice
//
//  Created by Randy Flores on 10/16/20.
//

import UIKit

struct Pokemon {
  var id : String
  var name : String
  var types : [String]
  var image : URL
  var height : String
  var weight : String
  var abilities : [String]
}
