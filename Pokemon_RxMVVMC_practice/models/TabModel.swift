//
//  TabModel.swift
//  Pokemon_RxMVVMC_practice
//
//  Created by Randy Flores on 10/16/20.
//

import UIKit

enum TabChildren: Int {
  case home
  case search
  
  var title: String? {
    switch self {
    case .home:
      return "Home"
    case .search:
      return "Search"
    }
  }
  
  var icon: UIImage? {
    switch self {
    case .home:
      return UIImage(named: "home")
    case .search:
      return UIImage(named: "search")
    }
  }
  
  var navigationController: UINavigationController {
    let navigation = UINavigationController()
    navigation.tabBarItem.title = self.title
    navigation.tabBarItem.image = self.icon
    return navigation
  }
}
